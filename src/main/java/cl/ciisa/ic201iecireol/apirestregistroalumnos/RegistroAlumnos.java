/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ciisa.ic201iecireol.apirestregistroalumnos;

import cl.ciisa.ic201iecireol.apirestregistroalumnos.dao.RegistroalumnosJpaController;
import cl.ciisa.ic201iecireol.apirestregistroalumnos.entity.Registroalumnos;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author davidbousquet
 */
@Path("registro")
public class RegistroAlumnos {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarRegistros() {
        RegistroalumnosJpaController dao = new RegistroalumnosJpaController();
        try {
            List<Registroalumnos> lista = dao.findRegistroalumnosEntities();
            return Response.ok(200).entity(lista).build();
        } catch (Exception ex) {
            return Response.serverError().build();
        }
    }
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(@PathParam("id") String id) {
        RegistroalumnosJpaController dao = new RegistroalumnosJpaController();
        try {
            Short idShort = Short.parseShort(id);
            Registroalumnos registroAlumno = dao.findRegistroalumnos(idShort);
            return Response.ok(200).entity(registroAlumno).build();
        } catch (Exception ex) {
            return Response.serverError().build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Registroalumnos registroAlumno) {
        RegistroalumnosJpaController dao = new RegistroalumnosJpaController();
        
        try {
            dao.create(registroAlumno);
            return Response.ok(200).entity(registroAlumno).build();
        } catch (Exception ex) {
            return Response.serverError().build();
        }
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") String id) {
        RegistroalumnosJpaController dao = new RegistroalumnosJpaController();
       
        try {
            Short idShort = Short.parseShort(id);
            dao.destroy(idShort);
            return Response.ok(204).build();
        } catch (Exception ex) {
            return Response.status(401).build();
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response edit(Registroalumnos registroAlumno) {
        //todo
        RegistroalumnosJpaController dao = new RegistroalumnosJpaController();
        try {
            dao.edit(registroAlumno);
            return Response.ok(200).entity(registroAlumno).build();
        } catch (Exception ex) {
            return Response.serverError().build();
        }
    }

}
