/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ciisa.ic201iecireol.apirestregistroalumnos.dao;

import cl.ciisa.ic201iecireol.apirestregistroalumnos.dao.exceptions.NonexistentEntityException;
import cl.ciisa.ic201iecireol.apirestregistroalumnos.entity.Registroalumnos;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author davidbousquet
 */
public class RegistroalumnosJpaController implements Serializable {

    public RegistroalumnosJpaController() {
        this.emf = emf;
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Registroalumnos registroalumnos) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(registroalumnos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Registroalumnos registroalumnos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            registroalumnos = em.merge(registroalumnos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Short id = registroalumnos.getId();
                if (findRegistroalumnos(id) == null) {
                    throw new NonexistentEntityException("The registroalumnos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Registroalumnos registroalumnos;
            try {
                registroalumnos = em.getReference(Registroalumnos.class, id);
                registroalumnos.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The registroalumnos with id " + id + " no longer exists.", enfe);
            }
            em.remove(registroalumnos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Registroalumnos> findRegistroalumnosEntities() {
        return findRegistroalumnosEntities(true, -1, -1);
    }

    public List<Registroalumnos> findRegistroalumnosEntities(int maxResults, int firstResult) {
        return findRegistroalumnosEntities(false, maxResults, firstResult);
    }

    private List<Registroalumnos> findRegistroalumnosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Registroalumnos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Registroalumnos findRegistroalumnos(Short id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Registroalumnos.class, id);
        } finally {
            em.close();
        }
    }

    public int getRegistroalumnosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Registroalumnos> rt = cq.from(Registroalumnos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
