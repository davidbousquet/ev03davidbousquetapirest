<%-- 
    Document   : index
    Created on : May 2, 2021, 7:52:29 PM
    Author     : davidbousquet
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>API Registro Alumnos - Evaluación 03 - TALLER DE APLICACIONES EMPRESARIALES - IC201IECIREOL</title>
    </head>
    <body>
        <h1>Descripción API:</h1>
        <h2>URL GET: https://registro-almunos.herokuapp.com/api/registro</h2>
        <hr>
        <h2>URL GET by ID: https://registro-almunos.herokuapp.com/api/registro/{{id}}</h2>
        <hr>
        <h2>URL POST: https://registro-almunos.herokuapp.com/api/registro</h2>
        <h3>Body POST:</h3>
        <code>    
            {
            "rut": "3232323",
            "nombre": "Lucia",
            "apellido": "Alvarez",
            "carrera": "Finanzas",
            "correo": "lalvarez@ciisa.cl"
            }
        </code>
        <hr>
        <h2>URL DELETE: https://registro-almunos.herokuapp.com/api/registro/{{id}}</h2>
        <hr>
        <h2>URL PUT: https://registro-almunos.herokuapp.com/api/registro/</h2>
        <h3>Body PUT:</h3>
        <code>    
            {
            "id": "5",
            "rut": "17123456-1",
            "nombre": "Lucia",
            "apellido": "Alvarez",
            "carrera": "Finanzas",
            "correo": "lalvarez@ciisa.cl"
            }
        </code>
    </body>
</html>
